package com.example.ulangansatu;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class Resultactivity extends AppCompatActivity {
    private TextView beforeResult;
    private TextView resultView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultactivity);
        beforeResult = findViewById(R.id.beforeResultTxtView);
        resultView = findViewById(R.id.resultTxtView);
        DoSetup();
    }

    private void DoSetup() {
        Bundle extras = getIntent().getExtras();
        String type = extras.getString("calcType");
        String panjang = extras.getString("panjang");
        String lebar = extras.getString("lebar");
        String tinggi = extras.getString("tinggi");
        String hasil = extras.getString("hasil");
        String constructedBefore = String.format("Hasil", type, panjang, lebar, tinggi);
        beforeResult.setText(constructedBefore);
        resultView.setText(hasil);
    }
}
